//
//  MenuTableViewController.swift
//  Kodobar
//
//  Created by Davor Šafranko on 4/10/17.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import UIKit


class MenuTableViewController: UITableViewController {
	var MenuGroups : [MenuGroup]? = nil
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.tableView.allowsSelection = false
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
		MenuGroups?.removeAll()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return (MenuGroups?.count)!
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ((MenuGroups?[section].getItems())?.count)!
    }

	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return MenuGroups?[section].getName()
	}
	
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sectionCell", for: indexPath) as! MenuTableViewCell

		let itemName = (MenuGroups?[indexPath.section].getItems())?[indexPath.row].getItemName()
        let itemPrice = (MenuGroups?[indexPath.section].getItems())?[indexPath.row].getItemPrice()
        let itemQuantity = (MenuGroups?[indexPath.section].getItems())?[indexPath.row].getItemQuantity()
		
        cell.quantityStepper.value = Double(itemQuantity!)
		cell.titleLabel.text = itemName
		cell.priceLabel.text = "TABLE_VIEW_CELL_PRICE".localized + " : \(itemPrice!) kn"
        cell.quantityLabel.text = String(itemQuantity!)
		cell.menuItem = (MenuGroups?[indexPath.section].getItems())?[indexPath.row]
		
		return cell
    }

}
