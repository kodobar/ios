//
//  QRCodeScannerViewController.swift
//  Kodobar
//
//  Created by Davor Šafranko on 4/2/17.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import UIKit
import AVFoundation
import MBProgressHUD

class QRCodeScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var viewfinderOverlay: UIView!
    
    @IBOutlet weak var helpView: UILabel!
	
    private var captureSession : AVCaptureSession?
	private var videoPreviewLayer : AVCaptureVideoPreviewLayer?
	
	private lazy var informationModal: UIAlertController = {
        let informationModal = UIAlertController(title: nil, message:"INFO_MODAL_MESSAGE".localized , preferredStyle: UIAlertControllerStyle.alert)
		let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in }
		informationModal.addAction(okAction)// Alert will not be nil because it was created above
		
		return informationModal
	}()

	@IBOutlet weak var InfoButton: UIButton!
	
	private func deserializeJSON(json : [[String : Any]]?) -> [MenuGroup]{
		var drinkGroups = [MenuGroup]()
		
		if let json = json{
			for group in json{
				let menuGroup = MenuGroup(json : group)
				drinkGroups.append(menuGroup)
			}
		}
		
		return drinkGroups
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		
		let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
		
		// Get an instance of the AVCaptureDeviceInput class
		var input : AnyObject?
		do {
			input = try AVCaptureDeviceInput.init(device: captureDevice)
		} catch _ as NSError  {
			print("No available input device!")
		}
		
		// Initialize capture session object
		captureSession = AVCaptureSession()
		captureSession?.addInput(input as! AVCaptureInput)
		
		// Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session
		let captureMetadataOutput = AVCaptureMetadataOutput()
		captureSession?.addOutput(captureMetadataOutput)
		
		// Set self as a delegate
		captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
		captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
		
		// Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer
		videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
		videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
		videoPreviewLayer?.frame = view.layer.bounds
		view.layer.addSublayer(videoPreviewLayer!)
        
        viewfinderOverlay.layer.cornerRadius = 10.0
        viewfinderOverlay.layer.borderColor = UIColor.white.cgColor
        viewfinderOverlay.layer.borderWidth = 2.0
        
        // Start capturing the video
		captureSession?.startRunning()
        
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        style.firstLineHeadIndent = 10.0
        style.headIndent = 20.0
        style.tailIndent = -20.0
        
        let attrText = NSAttributedString(string: "INFO_MODAL_MESSAGE".localized, attributes: [NSParagraphStyleAttributeName : style])
        helpView.attributedText = attrText
//        helpView.text = "INFO_MODAL_MESSAGE".localized
        helpView.layer.backgroundColor = UIColor(colorLiteralRed: 1.0, green: 1.0, blue: 1.0, alpha: 0.4).cgColor
        helpView.layer.cornerRadius = 10.0
        view.addSubview(ShadowOverlayView(outerFrame:self.view.bounds, innerFrame:viewfinderOverlay.frame, cornerRadius:viewfinderOverlay.layer.cornerRadius))
        self.viewDidLayoutSubviews()
        
//         :0.f green:0.f blue:0.f alpha:0.4f
    }
	
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       
        view.bringSubview(toFront: viewfinderOverlay)
        view.bringSubview(toFront: InfoButton)
        view.bringSubview(toFront: helpView)
    }
    
	override func viewWillAppear(_ animated: Bool) {
		// Hide Navigation Bar
		super.viewWillAppear(animated)
        
		navigationController?.setNavigationBarHidden(true, animated: true)
		
		if captureSession?.isRunning == false{
			captureSession?.startRunning()
		}
	}
	
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
	func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
		// Check if metadataObjects contains any data
		if(metadataObjects == nil || metadataObjects.count == 0){
			return
		}
		
		// Get the metadata object.
		let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
		
        if metadataObj.type == AVMetadataObjectTypeQRCode  && isMetadataObjectInViewfinder(metadataObject: metadataObj) {
			// If the found metadata is equal to the QR code metadata then set the bounds
			let qrCodeValue : String?
			if let metaDataValue = metadataObj.stringValue{
				qrCodeValue = metaDataValue
                // If information modal is showing, dismiss it
                captureSession?.stopRunning()

                informationModal.dismiss(animated: true, completion: nil)
				if(qrCodeValue != nil){
					fetchData(id :qrCodeValue!)
				}
			}
		}
	}
    
    private func isMetadataObjectInViewfinder(metadataObject:AVMetadataMachineReadableCodeObject)->Bool{
        let objectBounds = videoPreviewLayer?.transformedMetadataObject(for: metadataObject).bounds
        return viewfinderOverlay.frame.contains(objectBounds!);
    }
	
	private func fetchData(id : String){
		let newController : MenuOrderViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuOrderViewController") as! MenuOrderViewController
		
		let loadingHud = MBProgressHUD.showAdded(to: self.view, animated: true)
		loadingHud.label.text = "LOADING_MENU_MESSAGE".localized
		
		DispatchQueue.global(qos: .userInitiated).async { [weak self] in
			let networkSession = URLSession(configuration: URLSessionConfiguration.default)
			var dataTask : URLSessionDataTask
			let url = URL(string: "https://kodobar-api.herokuapp.com/api/public/menu/\(id)")
			
			// Let user know that network process is running
			UIApplication.shared.isNetworkActivityIndicatorVisible = true
			
			dataTask = networkSession.dataTask(with: url!){
				data, response, error in
				if error != nil{
					// Show Modal that something went wrong
					DispatchQueue.main.async {
						self?.informationModal.message = "NO_NETWORK_CONNECTION".localized
						self?.hideLoadingAndNetworkIndicator(for: loadingHud)
                        self?.present((self?.informationModal)!, animated: true, completion: { [weak self ] in
                            self?.captureSession?.startRunning()
                        })
					}
				}else if let httpResponse = response as? HTTPURLResponse{
					if httpResponse.statusCode == 200{
						DispatchQueue.main.async {
							// If we received some data
							if let data = data{
								// Serialize JSON data
								let json = try? JSONSerialization.jsonObject(with: data) as? [[String:Any]]
								
								if json != nil{
									// Get Drink Groups
									let drinkGroups = self?.deserializeJSON(json: json!)
									
									// Pass data to MenuOrderViewController
									if let drinkGroups = drinkGroups{
										let table = Table(tableId : id, menuGroups : drinkGroups)
										newController.table = table
										self?.hideLoadingAndNetworkIndicator(for: loadingHud)
										self?.navigationController?.pushViewController(newController, animated: true)
									}
								}else{
									// Show Network Modal
									self?.informationModal.message = "DOWNLOADING_DATA_ERROR".localized
									self?.hideLoadingAndNetworkIndicator(for: loadingHud)
                                    self?.present((self?.informationModal)!, animated: true, completion: {[weak self] in self?.captureSession?.startRunning()}
                                    )
								}
							}
						}
					}
				}
			}
			
			dataTask.resume()
        }
    }
	
	private func hideLoadingAndNetworkIndicator(for loadingHud: MBProgressHUD){
		UIApplication.shared.isNetworkActivityIndicatorVisible = false
		loadingHud.hide(animated: true)
	}
    
	@IBAction func infoButtonClicked(_ sender: UIButton) {
        
        UIView.transition(with: helpView, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: {
            self.helpView.isHidden = !self.helpView.isHidden
        }, completion: nil)
	}
    
}

