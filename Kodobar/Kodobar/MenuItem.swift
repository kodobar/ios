//
//  MenuItem.swift
//  Kodobar
//
//  Created by Davor Šafranko on 4/9/17.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import Foundation

class MenuItem {
	private let ID : String
	private let Name : String
	private let Price : Double
	private var Quantity : Int?
	
	init(json : [String : Any]){
		let drinkJSON = json["drink"] as? [String : Any]
		if let drinkJSON = drinkJSON{
			let id = drinkJSON["id"] as? String
			let name = drinkJSON["name"] as? String
			
			if let id = id{
				self.ID = id
			}else{
				self.ID = ""
			}
			
			if let name = name{
				self.Name = name
			}else{
				self.Name = ""
			}
		}else{
			self.Name = ""
			self.ID = ""
		}
		
		let price = json["price"] as? Double
		if let price = price{
			self.Price = price
		}else{
			self.Price = 0
		}
	}
	
	func getItemId() -> String{
		return ID
	}
	
	func getItemName() -> String{
		return Name
	}
	
	func getItemPrice() -> Double{
		return Price
	}
	
	func getItemQuantity() -> Int{
		if let quantity = Quantity{
			return quantity
		}
		
		return 0
	}
	
    func setQuantity(quantity : Int){
		Quantity = quantity
	}
}
