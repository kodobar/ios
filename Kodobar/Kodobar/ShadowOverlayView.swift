//
//  ShadowOverlayView.swift
//  Kodobar
//
//  Created by Jure Cular on 10/05/2017.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import UIKit

class ShadowOverlayView: UIView {
    
    private var innerFrame: CGRect!
    private var outerFrame: CGRect!
    private var cornerRadius: CGFloat!
    
    convenience init(outerFrame: CGRect, innerFrame: CGRect, cornerRadius: CGFloat) {
        self.init(frame: outerFrame)
        
        self.outerFrame = outerFrame
        self.innerFrame = innerFrame
        self.cornerRadius = cornerRadius
        
        self.isUserInteractionEnabled = false
        self.isOpaque = false
        
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        super.draw(rect)

        let framePath = UIBezierPath(rect: outerFrame)
        let viewfinderPath = UIBezierPath(roundedRect: innerFrame, cornerRadius: cornerRadius)
        let shadowColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
//
        framePath.append(viewfinderPath)
        framePath.usesEvenOddFillRule = true
        framePath.addClip()
        shadowColor.setFill()
        framePath.fill()
    }

}
