//
//  TableOrder.swift
//  Kodobar
//
//  Created by Davor Šafranko on 5/12/17.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import Foundation

class TableOrder{
	private let tableID : String
	private let orderItems : [MenuItem]

	init(tableID : String, orderItems : [MenuItem]){
		self.tableID = tableID
		self.orderItems = orderItems
	}
	
	func getTableID() -> String{
		return tableID
	}
	
	func getOrderItems() -> [MenuItem]{
		return orderItems
	}
}
