//
//  OrderItem.swift
//  Kodobar
//
//  Created by Davor Šafranko on 5/12/17.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import Foundation

class OrderItem{
	private var itemID : String
	private var quantity : Int
	
	init(itemId : String, quantity : Int){
		self.itemID = itemId
		self.quantity = quantity
	}
	
	func getJSON() -> [String : Any]?{
		let json : [String:Any] = ["drink" : itemID, "quantity" : quantity]
		return json
	}
}
