//
//  Order.swift
//  Kodobar
//
//  Created by Jure Cular on 09/05/2017.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import Foundation

class Order{
    private var drinks : [OrderItem]
    private var tableNumber : String
    
    init(drinks : [OrderItem], tableNumber : String){
        self.drinks = drinks
        self.tableNumber = tableNumber
    }
    
    func getJSON() -> [String : Any]?{
        var drinksJSON = [[String:Any]]()
        
        for drink in drinks{
            drinksJSON.append(drink.getJSON()!)
        }
        
        let json : [String:Any] = ["table": tableNumber, "drinks" : drinksJSON]
        return json
    }
}
