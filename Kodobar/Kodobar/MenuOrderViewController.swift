//
//  MenuOrderViewController.swift
//  Kodobar
//
//  Created by Davor Šafranko on 4/5/17.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import UIKit

class MenuOrderViewController: UIViewController {
	private let menuTabName = "MENU_TAB".localized
	private let orderTabName = "ORDER_TAB".localized
	var table : Table?
	
	enum TabIndex : Int{
		case menuTab = 0
		case orderTab = 1
	}
	
	private var segmentedControl : UISegmentedControl?
	@IBOutlet private weak var viewContent: UIView!// Content view that will change depending on SegmentedControl selection
	private var currentViewController : UIViewController?
	
	// Instantiate ViewControllers only if needed
	private lazy var menuTabViewController: MenuTableViewController? = {
		let menuTabViewController : MenuTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuTableViewController
		
		return menuTabViewController
	}()
	
	private lazy var orderTabViewController : OrderViewController? = {
        let orderViewController : OrderViewController = self.storyboard?.instantiateViewController(withIdentifier: "secondVC") as! OrderViewController
		
		let tableOrder = TableOrder(tableID: (self.table?.getTableID())!, orderItems: self.createOrderList())
        orderViewController.tableOrder = tableOrder
        
		return orderViewController
	}()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.automaticallyAdjustsScrollViewInsets = false
		// Initialize SegmentedControl
		segmentedControl = UISegmentedControl(items : [menuTabName, orderTabName])
		segmentedControl?.selectedSegmentIndex = TabIndex.menuTab.rawValue
		segmentedControl?.addTarget(self, action: #selector(switchTabs), for: .valueChanged)
		self.navigationItem.titleView = segmentedControl!
		displayCurrentlySelectedTab()
		
		// Set SegmentedControl in NavigationBar
		navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	func switchTabs(){
		self.currentViewController!.view.removeFromSuperview()
		self.currentViewController!.removeFromParentViewController()
		
		displayCurrentlySelectedTab()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		if let currentViewController = currentViewController {
			currentViewController.viewWillDisappear(animated)
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		self.navigationController!.navigationBar.backItem?.title = "BACK_BUTTON_TEXT".localized
	}
	
	private func displayCurrentlySelectedTab(){
		if let viewController = viewControllerForIndex( (segmentedControl?.selectedSegmentIndex)!) {
			
			self.addChildViewController(viewController)
			viewController.didMove(toParentViewController: self)
			
			viewController.view.frame = self.viewContent.bounds
			self.viewContent.addSubview(viewController.view)
			self.currentViewController = viewController
		}
	}
	
	private func viewControllerForIndex(_ index : Int) -> UIViewController?{
		switch index {
		case TabIndex.menuTab.rawValue :
			if menuTabViewController?.MenuGroups == nil{
				menuTabViewController?.MenuGroups = self.table?.getMenuGroups()
			}
			
			return menuTabViewController
		case TabIndex.orderTab.rawValue :
			let tableOrder = TableOrder(tableID: (table?.getTableID())!, orderItems: self.createOrderList())
            orderTabViewController?.tableOrder = tableOrder
			return orderTabViewController
		default:
			return nil
		}
	}
    
    private func createOrderList() -> [MenuItem] {
        var orderList : [MenuItem] = []
        
        for group in (self.table?.getMenuGroups())! {
            let items = group.getItems().filter{
                if let item = ($0 as MenuItem?) {return item.getItemQuantity() > 0}
                else {return false}
            }
            orderList.append(contentsOf: items)
        }
        
        return orderList
    }
}
