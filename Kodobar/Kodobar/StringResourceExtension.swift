//
//  StringResourceExtension.swift
//  Kodobar
//
//  Created by Davor Šafranko on 4/2/17.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import Foundation

extension String{
	var localized: String {
		return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
	}
}
