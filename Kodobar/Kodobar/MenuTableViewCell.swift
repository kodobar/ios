//
//  MenuTableViewCell.swift
//  Kodobar
//
//  Created by Jure Cular on 09/05/2017.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var quantityStepper: UIStepper!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    var menuItem : MenuItem?
        
    var quantity : Int = 0{
        didSet{
            menuItem?.setQuantity(quantity: quantity)
            quantityLabel.text = String(describing : quantity)
        }
    }
        
    @IBAction func didPressStepper(_ sender: UIStepper) {
        
        quantity = Int(sender.value)
        
    }
}
