//
//  OrderTableViewController.swift
//  Kodobar
//
//  Created by Jure Cular on 09/05/2017.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import UIKit
import MBProgressHUD
class OrderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var orderButton: UIButton!
    
    @IBOutlet weak var orderTableView: UITableView!
	
	public var tableOrder : TableOrder?
	
	private lazy var orderModal: UIAlertController = {
		let orderModal = UIAlertController(title: nil, message: "SUCCESSFULL_ORDER".localized, preferredStyle: UIAlertControllerStyle.alert)
		let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in }
		orderModal.addAction(okAction)// Alert will not be nil because it was created above
		
		return orderModal
	}()
	
    override func viewDidAppear(_ animated: Bool) {
        self.orderTableView.reloadData();

        let totalCount = tableOrder?.getOrderItems().reduce(0) {$0 + $1.getItemPrice() * Double($1.getItemQuantity())}
		
        self.priceLabel.text = "TOTAL_PRICE_LABEL".localized + String(format:"%.1f", totalCount!) + " kn"
        
        self.orderButton.isEnabled = (totalCount != 0)
        if (self.orderButton.isEnabled) {
            self.orderButton.backgroundColor = orderButton.tintColor
        } else {
            self.orderButton.backgroundColor = UIColor.lightGray
        }
    }
	
    override func viewDidLoad() {
        super.viewDidLoad()
        orderButton.setTitle("ORDER_BUTTON_TEXT".localized, for: .normal)
        orderButton.layer.cornerRadius = 5.0
		
		self.orderTableView.tableFooterView = UIView(frame: CGRect.zero)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let orderCount = tableOrder?.getOrderItems().count {return orderCount}
        return 0
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderTableViewCell", for: indexPath) as? OrderTableViewCell
        
        if let item = tableOrder?.getOrderItems()[indexPath.row] {
            cell?.titleLabel.text = item.getItemName()
			let totalPrice = Double(item.getItemQuantity()) * item.getItemPrice()
			
            cell?.priceLabel.text = "\(totalPrice) kn"
            cell?.quantityLabel.text = "TABLE_VIEW_QUANTITY".localized + " : \(item.getItemQuantity())"
        }
        
        return cell!
    }
	@IBAction func placeOrder() {
		var orderItems = [OrderItem]()
		
		if tableOrder?.getOrderItems() != nil{
			for item in (tableOrder?.getOrderItems())!{
				let orderItem = OrderItem(itemId: item.getItemId(), quantity: item.getItemQuantity())
				orderItems.append(orderItem)
			}
		}
		
		let order = Order(drinks: orderItems, tableNumber: (tableOrder?.getTableID())!)
		
		let loadingHud = MBProgressHUD.showAdded(to: self.view, animated: true)
		loadingHud.label.text = "LOADING_MENU_MESSAGE".localized
		UIApplication.shared.isNetworkActivityIndicatorVisible = true
		
		let url = URL(string: "https://kodobar-api.herokuapp.com/api/public/order")!
		var request = URLRequest(url: url)
		request.httpMethod = "PUT"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		// insert json data to the request
		
		let json: [String: Any] = order.getJSON()!		
		let jsonData = try? JSONSerialization.data(withJSONObject: json)
		
		request.httpBody = jsonData
		
		let task = URLSession.shared.dataTask(with: request) { data, response, error in
			DispatchQueue.main.async {
				UIApplication.shared.isNetworkActivityIndicatorVisible = false
				loadingHud.hide(animated: true)
				if error == nil{
					self.present((self.orderModal), animated: true, completion: nil)
				}else{
					self.orderModal.message = "UNSUCCESSFULL_ORDER".localized
					self.present((self.orderModal), animated: true, completion: nil)
				}
			}
		}
		
		task.resume()
	}
}
