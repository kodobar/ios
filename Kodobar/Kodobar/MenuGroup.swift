//
//  MenuGroup.swift
//  Kodobar
//
//  Created by Davor Šafranko on 4/10/17.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import Foundation

class MenuGroup {
	private let ID : String?
	private let Name : String?
	private var Items = [MenuItem]()
	
	init(json : [String : Any]){
		let name = json["name"] as? String
		let id = json["id"] as? String
		let drinks = json["drinks"] as? [[String : Any]]
		
		if let drinks = drinks{
			for drink in drinks{
				self.Items.append(MenuItem(json : drink))
			}
		}
		
		self.Name = name
		self.ID = id
	}
	
	func getName() -> String{
		return Name!
	}
	
	func getItems() -> [MenuItem]{
		return Items
	}
}
