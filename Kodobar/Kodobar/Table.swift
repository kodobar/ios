//
//  Table.swift
//  Kodobar
//
//  Created by Davor Šafranko on 5/12/17.
//  Copyright © 2017 Kodobar. All rights reserved.
//

import Foundation

class Table{
	private let tableID : String
	private var menuGroups : [MenuGroup]
	
	init(tableId : String, menuGroups : [MenuGroup]){
		self.tableID = tableId
		self.menuGroups = menuGroups
	}
	
	func getTableID()-> String{
		return self.tableID
	}
	
	func getMenuGroups()-> [MenuGroup]{
		return self.menuGroups
	}
}
